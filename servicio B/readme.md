# Servicio B

## Sinopsis del servicio B
Enviar solicitudes HTTP para la integración con servicios web. 
Interfaz sencilla para la construcción de cadenas de consulta, las peticiones 
POST y GET, la transmisión de grandes archivos, la carga de datos JSON, etc … para 
ser mas específicos, este servicio Permitirá la búsqueda de Esparcimiento 
culturales y deportivos, y otros servicios recreativos.

## Requerimientos para el servicio B
- php version 7.3.5: php es un lenguaje de código abierto especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML.
- composer version 2.1.3: Composer es un sistema de gestión de paquetes para programar en PHP el cual provee los formatos estándar necesarios para manejar dependencias y librerías de PHP.
- Laravel version 5.5: Laravel es un framework de código abierto para desarrollar aplicaciones y servicios web con PHP 5, PHP 7 y PHP 8. 
- Guzzle version 6.0: Guzzle es un cliente HTTP de PHP que facilita el envío de solicitudes HTTP y simplifica la integración con los servicios web.
 
## Uso: 
Para que el cliente pueda ver información general de esparcimientos culturales y deportivos, y otros servicios recreativos.

## Integrantes:
- Acevedo Sanchez Roberto Carlos
- Lorenzo Olivera Anselmo
