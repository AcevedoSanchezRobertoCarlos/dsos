## Título
Servicio Web sobre el DENUE de 2021

Integrantes del equito:
- Barroso Garcia Jhonatan
- Méndez Platas David Alejandro 

## Sinopsis
Con datos obtenidos de INEGI en formatos que se pueden utilizar de forma local en el equipo se ha desarrollado un servicio web que es una tecnología que utiliza un conjunto de protocolos y estándares que sirven para intercambiar datos entre aplicaciones.
Se ha utilizado el asistente de NetBeans para el desarrollo de dicha actividad por medio de Maven que es una herramienta de software para la gestión y construcción de proyectos Java el cual está alojado en Heroku para su utilización de forma remota.

